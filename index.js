/* ==========================================
 * GitArchive front (mainly landing & search)
 */

const path			= require("path");
const express			= require("express");
const app			= express();

const config			= require("./config");

/*
 *	Return current date
 */
function	getDate() {
	let now = new Date().getTime() + (2 * 60 * 60 * 1000);
	return (new Date(now).toISOString().replace(/T/, ' ').replace(/\..+/, ''));
}

/*
 * Gandi simple hosting related.
 * May be moved without worries
 * if app is not host on Gandi SH.
 */

if (global.gandi) { module.exports = app; app.listen = function () {}; }

/*
 * Express related params
 */

app.set('port', config.services.www.port);
app.set('view engine', 'pug');
app.set('views', path.join(__dirname, 'views'));

app.use(express.static(path.join(__dirname, 'public')))
app.get('/favicon.ico', (req, res) => { return res.redirect('/img/favicon/icon16.ico') })

app.locals.lib = {};
app.locals.lib.filesize = require('filesize');
app.locals.lib.moment = require('moment')
app.locals.lib.teaseHash = function (hash) {
	if (!hash)
		return (null);
	return hash.slice(0,7)
};

/**
 *	Express routes (Log request)
 */

app.use((req, res, next) => {
	const ipSource = req.headers['x-forwarded-for'] ||
	     req.connection.remoteAddress ||
	     req.socket.remoteAddress ||
	     (req.connection.socket ? req.connection.socket.remoteAddress : null);

	console.log("(" + getDate() + ")", "[WWW-GITARCHIVE]", "from:", ipSource, "(" + res.statusCode + ")", "[" + req.method + "]", req.originalUrl);
	return next();
});


require('./routes')(app);

/**
 *	Config
 */

app.listen(app.get('port')); // Listen port

console.log("(" + getDate() + ")", "[WWW-Gitarchive]", "Server started and connected ! API listen on port", app.get('port'));
