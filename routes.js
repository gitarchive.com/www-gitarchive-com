const config = require("./config");
const https = require("https");

/* MAIN */
module.exports = function (app) {
	app.get('/', (req, res) => { return res.render('index', {controllerName: "index"}) })
	app.get('/pricing', (req, res) => { res.render('pricing') })
	app.get('/support', (req, res) => { return res.redirect('https://twitter.com/frenchcooc'); })
	app.get('/resources', (req, res) => { return res.redirect('/'); })

	app.get('/explore', (req, res) => {

		// Fetch random resource information
		fetchAPI('/resources/random', (dataFromAPI) => {
			
			if (!dataFromAPI.object || dataFromAPI.object !== "resource")
				{ return res.status(500).render('errors/500'); }
			
			return res.redirect("/resources/" + dataFromAPI.id);
		});
	});

	app.get('/search', (req, res) => {
		let query = req.query.q
		
		if (!query)
			{ return res.redirect('/') }
		
		// Fetch resources that match query
		fetchAPI('/resources/search?q=' + encodeURIComponent(query) + '&limit=10', (dataFromAPI) => {
			
			if (!dataFromAPI.object || dataFromAPI.object !== "resources")
				{ return res.status(500).render('errors/500'); }
			
			return res.render('search', {query: req.query.q, dataFromAPI: dataFromAPI})
		});
	})
	
	app.get('/resources/:resource_id', (req, res) => {
		let resourceID = req.params.resource_id

		return res.redirect('/resources/'+resourceID+'/commits');
	})

	app.get('/resources/:resource_id/commits', (req, res) => {
		let resourceID = req.params.resource_id
		
		// Fetch resources information
		fetchAPI('/resources/' + resourceID, (ResourceItem) => {

			if (!ResourceItem.object || ResourceItem.object !== "resource")
				{ return res.status(500).render('errors/404'); }
			
			// Fetch all commits for this resource
			fetchAPI('/resources/' + resourceID + '/commits', (CommitsList) => {
				
				if (!CommitsList.object || CommitsList.object !== "commits")
					{ return res.status(500).render('errors/500'); }
			
				// if (!CommitsList.data.length)
				// 	{ return res.status(404).render('errors/404'); }
				
				return res.render('resources/commits', {
					controllerName: "commits",
					resource: ResourceItem,
					commits: CommitsList.data
				});
			});
		});
	})
	
	app.get('/resources/:resource_id/commits/:commit_id', (req, res) => {
		let resourceID = req.params.resource_id,
			commitID = req.params.commit_id
		
		// Fetch resource information
		fetchAPI('/resources/' + resourceID, (ResourceItem) => {

			if (!ResourceItem.object || ResourceItem.object !== "resource")
				{ return res.status(500).render('errors/404'); }

			// Fetch commit information
			fetchAPI('/resources/' + resourceID + '/commits/' + commitID, (CommitItem) => {
				
				if (!CommitItem.object || CommitItem.object !== "commit")
					{ return res.status(500).render('errors/500'); }

				return res.render('resources/raw', {
					controllerName: "raw",
					resource: ResourceItem,
					commit: CommitItem
				});
			});
		});
	})
	
	app.get('/resources/:resource_id/raw', (req, res) => {
		let resourceID = req.params.resource_id;

		// Fetch resources commits
		fetchAPI('/resources/' + resourceID + '/commits', (dataFromAPI) => {

			if (!dataFromAPI.object || dataFromAPI.object !== "commits")
				{ return res.status(500).render('errors/500'); }

			if (!dataFromAPI.data.length) 
				{ return res.redirect('/resources/'+resourceID+'/commits') }

			return res.redirect('/resources/'+resourceID+'/commits/'+dataFromAPI.data[0].id);
		});
	})

	app.get('/resources/:resource_id/diff/:from_commit_id\.\.:to_commit_id', (req, res) => {
		
		const 	resourceID = req.params.resource_id,
				fromCommitId = req.params.from_commit_id,
				toCommitId = req.params.to_commit_id;

		// Fetch resource information
		fetchAPI('/resources/' + resourceID, (ResourceItem) => {

			if (!ResourceItem.object || ResourceItem.object !== "resource")
				{ return res.status(500).render('errors/404'); }

			// Fetch resources commits
			fetchAPI('/resources/' + resourceID + '/diff/' + fromCommitId + '..' + toCommitId + "/body", (DiffItem) => {
				
				if (!DiffItem.object || DiffItem.object !== "diff")
					{ return res.status(500).render('errors/500'); }
				
				return res.render('resources/diff', {
					controllerName: "diff",
					resource: ResourceItem,
					fromCommitID: fromCommitId,
					toCommitID: toCommitId,
					diff: DiffItem
				});
			});
		});
	})
	
	app.get('/resources/:resource_id/activity', (req, res) => {
		let resourceID = req.params.resource_id;
		
		// TODO : show URL activity (fetchs & commits)
		fetchAPI('/resources/' + resourceID + '/activity', () => {
			return res.render('resources/activity', {
				controllerName: "activity",
				resourceID: resourceID
			});
		});
	});

	// Handle 404
	app.use(function(req, res, next) {
		res.status(404).render('errors/404');
	});
}

/* FETCH API */

function fetchAPI (path, callback) {

	// Prepare API call.
	const basicAuthKey = Buffer.from(config.services.api.token + ":").toString("base64");
	const options = {
		port: config.services.api.port,
		hostname: config.services.api.hostname,
		path: "/v" + config.services.api.version.toString() + path,
		agent: false,
		headers: {"Authorization": "Basic" + " " + basicAuthKey},
	};

	// Make HTTP request :)
	return makeRequest(options, callback);
}

function makeRequest (options, callback, loop = false) {
	
	// Overpass unsecure SSL certificate
	process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';
	console.error("Warning! - NODE_TLS_REJECT_UNAUTHORIZED is set to 0. This is unsecure.")
	
	try {
		var request = https.get(options, res => {
			// res.setEncoding("utf8");
			let bodyResponse = "";

			if (res.statusCode === 302) {
				let newOptions = options;
				newOptions.path = res.headers["location"];
				if (!loop) { return makeRequest(newOptions, callback, true); }
			}
			
			res.on("data", data => { bodyResponse += data; });
			res.on("end", () => {
				
				let jsonResponse = {};
				try {
					jsonResponse = JSON.parse(bodyResponse);
					
					if (jsonResponse.statusCode >= 200 && jsonResponse.statusCode < 300)
						{ return callback(jsonResponse); }

					console.error("Error:", jsonResponse);
					return callback({error: true, code: 501, message: "Unsupported response from API."});
				}
				catch (e) { console.error("Error:", e); return callback({error: true, code: 404, message: "No response from API."}); }
			});
		})
		request.on("error", (e) => { console.error("Error:", e); return callback({error: true, code: 500, message: "Unable to contact API."}); })
	}
	catch (e) { console.error("Error:", e); return callback({error: true, code: 500, message: "Unable to contact API."}); }
}
